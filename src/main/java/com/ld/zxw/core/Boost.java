//package com.ld.zxw.core;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.List;
//import java.util.UUID;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.log4j.Logger;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.ld.zxw.config.LuceneDataSource;
//import com.ld.zxw.page.Page;
//import com.ld.zxw.util.BoostDto;
//
///**
// * 动态权重控制
// * @author Administrator
// *
// */
//@WebServlet("/boost")
//public class Boost extends HttpServlet{
//	private static final long serialVersionUID = 1L;
//
//	Logger log = Logger.getLogger(Boost.class);
//
//	@Override
//	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		doPost(req, resp);
//	}
//
//	@Override
//	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		String method = req.getParameter("method");
//		if (method != null) {
//			switch (method) {
//			case "list":
//				list(req, resp);
//				req.getRequestDispatcher("view/boost/list.jsp").forward(req, resp);
//				break;
//			case "save":
//				save(req, resp);
//				sendRedirect(resp, "boost?method=list");
//				break;
//			case "update":
//				update(req, resp);
//				break;
//			case "delete":
//				delete(req, resp);
//				sendRedirect(resp, "boost?method=list");
//				break;
//			default:
//				resp.sendRedirect("view/404.jsp");
//				break;
//			}
//		}else{
//			resp.sendRedirect("view/index.jsp");
//		}
//	}
//
//	/**
//	 * 集合查询
//	 * @param req
//	 * @param resp
//	 */
//	public void list(HttpServletRequest req, HttpServletResponse resp){
//		String searchName = req.getParameter("searchName");
//		List<BoostDto> orObj = orObj(LuceneDataSource.build().jedis.lrange("LuceneBoost", 0, Integer.MAX_VALUE),searchName);
//		int pageNumber = Integer.parseInt(req.getParameter("pageNumber") == null ? "1" : req.getParameter("pageNumber"));
//		int pageSize = Integer.parseInt(req.getParameter("pageSize") == null ? "5" : req.getParameter("pageSize"));
//
//		int totalHits = orObj.size();
//		int totalPage = (int) (totalHits / pageSize);
//		if (totalHits % pageSize != 0) {
//			totalPage++;
//		}
//		int end = pageSize*pageNumber;
//		if(orObj.size() < end){
//			end = orObj.size();
//		}
//		Page<BoostDto> page = new Page<>(orObj.subList((pageNumber-1)*pageSize, end), pageNumber, pageSize, totalPage, totalHits);
//		req.setAttribute("page", page);
//		req.setAttribute("searchName",searchName);
//		log.info("查询");
//	}
//	/**
//	 * 添加
//	 * @param req
//	 * @param resp
//	 */
//	public void save(HttpServletRequest req, HttpServletResponse resp){
//		String name = req.getParameter("name");
//		String boost = req.getParameter("boost");
//		String creator = req.getParameter("creator");
//		BoostDto thesaurusDto = new BoostDto();
//		thesaurusDto.setId(UUID.randomUUID().toString());
//		thesaurusDto.setName(name);
//		thesaurusDto.setBoost(Float.valueOf(boost));
//		thesaurusDto.setCreator(creator);
//		thesaurusDto.setCreateTime(new Date());
//		thesaurusDto.setUpdateTime(new Date());
//		thesaurusDto.setStatus(1);
//		LuceneDataSource.build().jedis.lpush("LuceneBoost", JSON.toJSONString(thesaurusDto));
//		log.info("添加");
//	}
//	public void update(HttpServletRequest req, HttpServletResponse resp){
//		List<String> IkThesaurus = LuceneDataSource.build().jedis.lrange("LuceneBoost", 0, Integer.MAX_VALUE);
//		//清空之前正式库
//		LuceneDataSource.build().jedis.ltrim("LuceneBoost_LD", 1, 0);
//		int size = IkThesaurus.size();
//		boolean flag = true;
//		for (int i = 0; i < size; i++) {
//			try {
//				LuceneDataSource.build().jedis.lpush("LuceneBoost_LD",IkThesaurus.get(i));
//			} catch (Exception e) {
//				LuceneDataSource.build().jedis.ltrim("LuceneBoost_LD", 1, 0);
//				flag = false;
//				log.error("添加异常数据回滚", e);
//				break;
//			}
//		}
//		if(flag){
//			sendRedirect(resp, "view/boost/update.jsp");
//		}else{
//			sendRedirect(resp, "view/500.jsp");
//		}
//		log.info("更新");
//	}
//	/**
//	 * 删除
//	 * @param req
//	 * @param resp
//	 */
//	public void delete(HttpServletRequest req, HttpServletResponse resp){
//		String index = req.getParameter("index");
//		long k = Long.valueOf(index);
//		LuceneDataSource.build().jedis.lset("LuceneBoost", k, "__deleted__");
//		LuceneDataSource.build().jedis.lrem("LuceneBoost", 0, "__deleted__");
//		log.info("删除");
//	}
//	public List<BoostDto> orObj(List<String> list, String searchName){
//		List<BoostDto> dtos = new ArrayList<>();
//		int size = list.size();
//		if(searchName != null){
//			for (int i = 0; i < size; i++) {
//				BoostDto parseObject = JSONObject.parseObject(list.get(i), BoostDto.class);
//				String name = parseObject.getName();
//				if(name.indexOf(searchName) != -1){
//					dtos.add(parseObject);
//				}
//			}
//		}else{
//			for (int i = 0; i < size; i++) {
//				dtos.add(JSONObject.parseObject(list.get(i), BoostDto.class));
//			}
//		}
//		return dtos;
//	}
//
//	public List<String> orStr(List<BoostDto> list){
//		List<String> dtos = new ArrayList<>();
//		int size = list.size();
//		for (int i = 0; i < size; i++) {
//			dtos.add(JSON.toJSONString(list.get(i)));
//		}
//		return dtos;
//	}
//
//	public void sendRedirect(HttpServletResponse resp,String url){
//		try {
//			resp.sendRedirect(url);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//
//	public static void main(String[] args) {
//
//		List<Integer> asList = Arrays.asList(1,2,3,4,5,6,7,8,9);
//		System.out.println(asList.subList(1*5, (5*2)-1));
//
//	}
//
//}
