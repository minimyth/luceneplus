package com.ld.zxw.core;

import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.SortField.Type;

public class FieldSort {
	
	private Sort sort;
	
	public FieldSort() {
		sort = new Sort();
	}
	public void put(String field,Type type,boolean reverse) {
		sort.setSort(new SortField(field, type, reverse));
	}
	
	public Sort getSort() {
		return sort;
	}

}
