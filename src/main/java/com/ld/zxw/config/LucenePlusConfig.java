package com.ld.zxw.config;


import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.search.IndexSearcher;

public class LucenePlusConfig{
	
	//分词器选择
	
	private PerFieldAnalyzerWrapper perFieldAnalyzerWrapper = null;
	
	private boolean highlight = false;
	
	private boolean devMode = false;
	
	/**
	 * 默认开发模式 false  生产模式 为 true
	 * @param devMode
	 */
	public void setDevMode(boolean devMode) {
		this.devMode = devMode;
	}
	public boolean getDevMode() {
		return this.devMode;
	}

	//高亮显示
	private String[]  highlightConf = new String[]{"<font color='red'>","</font>"};
	
	//扩展词库路径  启用词
	private String  extWordPath;
	//扩展词库路径  停用词
	private String  stopWordPath;
	
//	private Highlighter highlighter;
//	
//	public Highlighter getHighlighter() {
//		return highlighter;
//	}
//	public void setHighlighter(Highlighter highlighter) {
//		this.highlighter = highlighter;
//	}
	
	
	//索引根目录
	private String  lucenePath = null;

	//索引目录
	private Path  path = null;
	
	//分词字段
	private List<String> participleField;
	
	//权重字段
	private Map<String, Float> boostField;
	
	//高亮字段
	private List<String> highlightFields;
	
	private IndexSearcher indexSearcher;
	
	private IndexWriter indexWriter;
	
	
	
	public IndexWriter getIndexWriter() {
		return indexWriter;
	}

	public void setIndexWriter(IndexWriter indexWriter) {
		this.indexWriter = indexWriter;
	}

	public IndexSearcher getIndexSearcher() {
		return indexSearcher;
	}

	public void setIndexSearcher(IndexSearcher indexSearcher) {
		this.indexSearcher = indexSearcher;
	}

	public List<String> getHighlightFields() {
		return highlightFields;
	}

	public void setHighlightFields(List<String> highlightFields) {
		this.highlightFields = highlightFields;
	}

	public boolean isHighlight() {
		return highlight;
	}

	public Map<String, Float> getBoostField() {
		return boostField;
	}

	public void setBoostField(Map<String, Float> boostField) {
		this.boostField = boostField;
	}

	public List<String> getParticipleField() {
		return participleField;
	}

	public void setParticipleField(List<String> participleField) {
		this.participleField = participleField;
	}


	public void setHighlight(boolean highlight) {
		this.highlight = highlight;
	}
	
	public PerFieldAnalyzerWrapper getAnalyzer() {
		return perFieldAnalyzerWrapper;
	}

	public void setAnalyzer(PerFieldAnalyzerWrapper perFieldAnalyzerWrapper) {
		this.perFieldAnalyzerWrapper = perFieldAnalyzerWrapper;
	}

	public String[] getHighlightConf() {
		return highlightConf;
	}

	public void setHighlightConf(String[] highlightConf) {
		this.highlightConf = highlightConf;
	}

	public String getLucenePath() {
		return lucenePath;
	}

	public void setLucenePath(String lucenePath) {
		this.lucenePath = lucenePath;
	}
	
	public Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	public PerFieldAnalyzerWrapper getPerFieldAnalyzerWrapper() {
		return perFieldAnalyzerWrapper;
	}

	public void setPerFieldAnalyzerWrapper(PerFieldAnalyzerWrapper perFieldAnalyzerWrapper) {
		this.perFieldAnalyzerWrapper = perFieldAnalyzerWrapper;
	}

	public String getExtWordPath() {
		return extWordPath;
	}

	public void setExtWordPath(String extWordPath) {
		this.extWordPath = extWordPath;
	}

	public String getStopWordPath() {
		return stopWordPath;
	}

	public void setStopWordPath(String stopWordPath) {
		this.stopWordPath = stopWordPath;
	}

	
}
